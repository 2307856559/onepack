package com.igo.onepack.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class OnePackProperties {

    private static Properties sProperties;

    private OnePackProperties() {}

    public static String getProperty(String key) {
        return get().getProperty(key);
    }

    private static Properties get() {
        if (sProperties == null) {
            load();
        }
        return sProperties;
    }

    private static void load() {
        try {
            InputStream is = OnePackProperties.class.getClassLoader().getResourceAsStream("onepack.properties");
            Properties properties = new Properties();
            properties.load(is);
            sProperties = properties;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}