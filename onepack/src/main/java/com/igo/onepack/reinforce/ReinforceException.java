package com.igo.onepack.reinforce;

import com.igo.onepack.OnePackException;

public class ReinforceException extends OnePackException {

    public ReinforceException(String message) {
        super(message);
    }

    public ReinforceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReinforceException(Throwable cause) {
        super(cause);
    }
}
