package com.igo.onepack.utils;

import java.io.PrintStream;
import java.text.SimpleDateFormat;

public class Log {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void d(String tag, String msg) {
        print(tag,msg,false);
    }

    public static void e(String tag, String msg) {
        print(tag,msg,true);
    }

    public static void e(String tag, String msg, Throwable e) {
        print(tag,msg,true);
        e.printStackTrace(System.err);
    }

    private static void print(String tag, String msg, boolean error) {
        String logTime = DATE_FORMAT.format(System.currentTimeMillis());
        final PrintStream printStream = error ? System.err : System.out;
        printStream.println(String.format("[%s] %s: %s", logTime, tag, msg));
    }
}
