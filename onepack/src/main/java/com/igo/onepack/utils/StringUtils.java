package com.igo.onepack.utils;

public class StringUtils {

    private StringUtils() {}

    public static boolean isEmpty(String text) {
        return text == null || text.length() == 0;
    }

    public static boolean noEmpty(String text) {
        return text != null && text.length() > 0;
    }
}
