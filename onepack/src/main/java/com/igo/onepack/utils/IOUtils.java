package com.igo.onepack.utils;

import java.io.Closeable;
import java.io.IOException;

public class IOUtils {

    private IOUtils() {}

    public static void closeQuietly(Closeable is) {
        if (is != null) {
            try {
                is.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
