package com.igo.onepack.channel;

import com.igo.onepack.utils.FileUtils;

import java.io.File;

public abstract class MultiChannel {

    private ChannelConfig channelConfig;
    private SignConfig signConfig;
    private File inputApkFile;
    private File outDir;

    public void setChannelConfig(ChannelConfig channelConfig) {
        this.channelConfig = channelConfig;
    }

    public ChannelConfig getChannelConfig() {
        return channelConfig;
    }

    public void setSignConfig(SignConfig signConfig) {
        this.signConfig = signConfig;
    }

    public SignConfig getSignConfig() {
        return signConfig;
    }

    public void setInputApkFile(File inputApkFile) {
        this.inputApkFile = inputApkFile;
    }

    public File getInputApkFile() {
        return inputApkFile;
    }

    public void setOutDir(File outDir) {
        this.outDir = outDir;
    }

    public File getOutDir() {
        return outDir;
    }

    public final void pack() throws ChannelException {
        if (!FileUtils.isFile(inputApkFile)) {
            throw new ChannelException("input apk invalid.");
        }
        if (outDir == null) {
            throw new ChannelException("out dir can not null.");
        }
        doPackMultiChannel(inputApkFile, channelConfig);
    }

    protected abstract void doPackMultiChannel(File apkFile, ChannelConfig channelConfig) throws ChannelException;
}