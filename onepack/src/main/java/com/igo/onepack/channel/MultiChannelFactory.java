package com.igo.onepack.channel;

import java.io.File;

public final class MultiChannelFactory {

    private MultiChannelFactory() {}

    public static MultiChannel createDefault() {
        return new ApkToolMultiChannel();
    }
}