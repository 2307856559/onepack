package com.igo.onepack;

public class OnePackException extends Exception {
    public OnePackException() {
        super();
    }

    public OnePackException(String message) {
        super(message);
    }

    public OnePackException(String message, Throwable cause) {
        super(message, cause);
    }

    public OnePackException(Throwable cause) {
        super(cause);
    }
}
