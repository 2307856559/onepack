package com.igo.onepack.utils;

import com.igo.onepack.channel.SignConfig;
import com.igo.onepack.utils.CMDUtils;
import com.igo.onepack.utils.FileUtils;
import com.igo.onepack.utils.Log;

import java.io.File;

public class SignHelper {

    private static final String TAG = "SignHelper";
    private SignConfig signConfig;
    private final String channel;
    private final String apkSignerJar;
    private boolean deleteOriginApk;

    public SignHelper(SignConfig signConfig) {
        this(signConfig,"");
    }

    public SignHelper(SignConfig signConfig, String channel) {
        this.signConfig = signConfig;
        this.channel = channel;
        this.apkSignerJar = getApkSignerJarPath();
    }

    public static String getApkSignerJarPath() {
        return System.getenv("APK_SIGNER_JAR_DIR");
    }

    public void setDeleteOriginApk(boolean deleteOriginApk) {
        this.deleteOriginApk = deleteOriginApk;
    }

    public File signApk(File apkFile) {
        apkFile = zipalign(apkFile);
        apkFile = sign(apkFile);
        return apkFile;
    }

    /**
     * 对齐Apk
     * @param apkFile apkFile
     */
    private File zipalign(File apkFile) {
        try {
            File outFile = new File(apkFile.getParentFile(), FileUtils.makeApkName(apkFile,"zipalign"));
            CMDUtils.exec(new CMDUtils.Callback() {
                @Override
                public void onStartExec() {
                    Log.d(TAG,String.format("%s apk start zipalign",channel));
                }

                @Override
                public void onStopExec() {
                    Log.d(TAG,String.format("%s apk stop zipalign",channel));
                }
            },"zipalign","-v","-p","4", apkFile.getAbsolutePath(),outFile.getAbsolutePath());
            return outFile;
        } finally {
            if (deleteOriginApk) {
                FileUtils.delete(apkFile);
            }
        }
    }

    private File sign(File apkFile) {
        try {
            File outFile = new File(apkFile.getParentFile(),FileUtils.makeApkName(apkFile,"sign"));
            CMDUtils.exec(new CMDUtils.Callback() {
                              @Override
                              public void onStartExec() {
                                  Log.d(TAG,String.format("%s apk start sign",channel));
                              }

                              @Override
                              public void onStopExec() {
                                  Log.d(TAG,String.format("%s apk finish sign",channel));
                              }
                          }, "java","-jar",apkSignerJar,"sign","--ks",signConfig.getKsPath(),"--ks-key-alias",signConfig.getKeyAlias(),
                    "--ks-pass","pass:"+signConfig.getKsPass(),"--key-pass","pass:"+signConfig.getKeyPass(),"--v1-signing-enabled","true",
                    "--v2-signing-enabled","true","--out",outFile.getAbsolutePath(),apkFile.getAbsolutePath());
            return outFile;
        } finally {
            FileUtils.delete(apkFile);
        }
    }
}