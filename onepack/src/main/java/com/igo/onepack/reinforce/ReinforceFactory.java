package com.igo.onepack.reinforce;

import com.igo.onepack.utils.Utils;

/**
 * 加固对象工厂创建
 */
public final class ReinforceFactory {

    public static AbstractReinforce createDefault(String propertiesName) {
        return new LeguReinforce(Utils.getConfig(propertiesName));
    }
}