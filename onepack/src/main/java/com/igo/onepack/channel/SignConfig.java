package com.igo.onepack.channel;

public class SignConfig {

    private String ksPath;
    private String keyAlias;
    private String ksPass;
    private String keyPass;

    public String getKsPath() {
        return ksPath;
    }

    public void setKsPath(String ksPath) {
        this.ksPath = ksPath;
    }

    public String getKeyAlias() {
        return keyAlias;
    }

    public void setKeyAlias(String keyAlias) {
        this.keyAlias = keyAlias;
    }

    public String getKsPass() {
        return ksPass;
    }

    public void setKsPass(String ksPass) {
        this.ksPass = ksPass;
    }

    public String getKeyPass() {
        return keyPass;
    }

    public void setKeyPass(String keyPass) {
        this.keyPass = keyPass;
    }
}