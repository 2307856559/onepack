package com.igo.onepack.reinforce;

import com.igo.onepack.utils.FileUtils;
import com.igo.onepack.utils.Log;

import java.io.File;

public abstract class AbstractReinforce {

    private static final String TAG = "AbstractReinforce";
    private File outDir;
    private File originApkFile;

    public File getOutDir() {
        return outDir;
    }

    public void setOutDir(File outDir) {
        this.outDir = outDir;
    }

    public File getOriginApkFile() {
        return originApkFile;
    }

    public void setOriginApkFile(File originApkFile) {
        this.originApkFile = originApkFile;
    }

    public final String getOriginApkFileName() {
        return FileUtils.getFileName(originApkFile);
    }

    /**
     * 加固方法
     * @return 返回加固好的apk file
     */
    public final File reinforce() throws ReinforceException {
        if (!FileUtils.isFile(originApkFile)) {
            Log.e(TAG,"Apk file invalid");
            return null;
        }
        if (outDir == null) {
            throw new ReinforceException("reinforce out dir can not null.");
        }
        return onReinforce(originApkFile);
    }

    protected abstract File onReinforce(File apkFile) throws ReinforceException ;
}