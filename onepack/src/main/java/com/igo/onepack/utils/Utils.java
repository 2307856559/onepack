package com.igo.onepack.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class Utils {

    private static final String TAG = "Utils";
    private Utils() {}

    public static Properties getConfig(String propertiesName) {
        Properties properties = new Properties();
        InputStream is = Utils.class.getClassLoader().getResourceAsStream(propertiesName);
        try {
            properties.load(is);
        } catch (IOException e) {
            Log.d(TAG,e.getMessage());
        }
        return properties;
    }

    public static Process exec(String cmd) {
        try {
            return Runtime.getRuntime().exec(cmd);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
