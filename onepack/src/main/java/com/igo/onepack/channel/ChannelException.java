package com.igo.onepack.channel;

import com.igo.onepack.OnePackException;

public class ChannelException extends OnePackException {

    public ChannelException() {
        super();
    }

    public ChannelException(String message) {
        super(message);
    }

    public ChannelException(String message, Throwable cause) {
        super(message, cause);
    }
}